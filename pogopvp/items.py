# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class PogopvpItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()                   #name of pokemon i.e bulbasaur
    id = scrapy.Field()                     #id of pokemon i.e 1

    type1 = scrapy.Field()                  #type: i.e grass
    type2 = scrapy.Field()                  #type2: i.e poison

    typeAdvantages = scrapy.Field()         #pokemon types it has advantage against i.e electric, fight, fairy, ice, grass
    typeDisadvantages = scrapy.Field()      #pokemon types it has disadvantage against i.e fire, flying, ice, psychic

    greatRank = scrapy.Field()              #i.e 4.5
    ultraRank = scrapy.Field()              #i.e 3.5
    masterRank = scrapy.Field()             #i.e 2

    bestMoveCombos = scrapy.Field()           #i.e Vine Whip + Frenzy Plant and Sludge Bomb (figure out move types quick/charge after)

    url = scrapy.Field()                    #url to page on gamespress.gg

    lastUpdated = scrapy.Field()            #date of last update

    '''

    #allFastMoves = scrapy.Field()           #i.e Razor Leaf, Vine Whip
    #allChargeMoves = scrapy.Field()         #i.e Solar Beam, etc. (include Elite Charge Moves), Frenzy Plant
    #family = scrapy.Field()                 #i.e bulbasaur, venusaur
    '''
