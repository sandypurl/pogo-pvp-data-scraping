import scrapy
from pogopvp.items import PogopvpItem
from datetime import datetime
import re

'''note: Hypno bestMoveCombos has commas in it, edit manually'''

'''run: scrapy crawl my_scraper -o writefilename'''


class Pogopvp(scrapy.Spider):
    name = "my_scraper"

    # First Start Url
    '''real one'''
    start_urls = ["https://gamepress.gg/pokemongo/pokemongo/pokemongo/pokemongo/pokemongo/pokemon-list"]

    #new
    def parse(self, response):

        count = -1000
        maxLoops = 1000

        '''use this block when getting data for real'''
        ''''''

        '''"//tr[contains(@class, 'pokemon-row')]//@data-id"'''
        for pid in response.xpath("//tr[contains(@class, 'pokemon-row')]/td[2]/a[2]//@href"):

            if count<=maxLoops:
            # add the scheme, eg http://

                # https://gamepress.gg/pokemongo/pokemon/
                url  = "https://gamepress.gg" + pid.extract()
                yield scrapy.Request(url, callback=self.parse_dir_contents)
                count+=1
            else:
                break

    def parse_dir_contents(self, response):

        item = PogopvpItem()

        #Pokemon name
        name = ""
        if response.xpath("//div[contains(@id, 'page-title')]/h1/descendant::text()").get() is None:
            item['name'] = name
        else:
            item['name'] = response.xpath("//div[contains(@id, 'page-title')]/h1/descendant::text()").extract()#[0].strip()

        pageUrl = ""
        id = ""
        if response.xpath("//div[contains(@id, 'block-gamepressbase-content')]/article[1]/@about").get() is None:
            item['id'] = id
            item['url'] = pageUrl
        else:
            pageUrl = response.xpath("//div[contains(@id, 'block-gamepressbase-content')]/article[1]/@about").extract()
            url = "https://gamepress.gg"+pageUrl[0]

            #Pokemon ID # (same w/ strings bc they have variations)
            item['id'] = url.strip().split("/")[-1]
            #url where info was found
            item['url'] = url

        #Pokemon type(s)
        types = []

        if response.xpath("//div[contains(@class, 'top-pokemon-stats')]//div[contains(@class, 'field field--name-field-type-image field--type-image field--label-hidden field__item')]//img[contains(@width, '32')]/@src").get() is None:
            item['type1'] = ""
            item['type2'] = ""
        else:
            for src in response.xpath("//div[contains(@class, 'top-pokemon-stats')]//div[contains(@class, 'field field--name-field-type-image field--type-image field--label-hidden field__item')]//img[contains(@width, '32')]/@src"):
                types.append(src.extract())

            if len(types)==2:
                item['type1'] = types[0].strip().split("/")[-1][:-4]
                item['type2'] = types[1].strip().split("/")[-1][:-4]
            if len(types)==1:
                item['type1'] = types[0].strip().split("/")[-1][:-4]
                item['type2'] = ""

        #types that the pokemon resists, so has an advantage over
        resistTypes = ""
        if response.xpath("//table[contains(@id, 'resist-table')]//td[contains(@class, 'type-img-cell')]//img/@src").get() is None:
            item['typeAdvantages'] = []
        else:
            for src in response.xpath("//table[contains(@id, 'resist-table')]//td[contains(@class, 'type-img-cell')]//img/@src"):
                type = src.extract().strip().split("/")[-1][:-4]
                resistTypes=resistTypes+"&"+type

            item['typeAdvantages'] = resistTypes[1:]

        #types that the pokemon is wead against, so is vulnerable
        vulnerableTypes = ""
        if response.xpath("//table[contains(@id, 'weak-table')]//td[contains(@class, 'type-img-cell')]//img/@src").get() is None:
            item['typeDisadvantages'] = []
        else:
            for src in response.xpath("//table[contains(@id, 'weak-table')]//td[contains(@class, 'type-img-cell')]//img/@src"):
                type = src.extract().strip().split("/")[-1][:-4]
                vulnerableTypes=vulnerableTypes+"&"+type

            item['typeDisadvantages'] = vulnerableTypes[1:]

        #Determine whether the Pokemon is ranked in PVP
        ratingsExist = 0
        subheadings = []

        if response.xpath("//div[contains(@id, 'block-gamepressbase-content')]//h2[contains(@class, 'main-title')]/descendant::text()").get() is None:
            item['greatRank'] = ""
            item['ultraRank'] = ""
            item['masterRank'] = ""
        else:
            for title in response.xpath("//div[contains(@id, 'block-gamepressbase-content')]//h2[contains(@class, 'main-title')]/descendant::text()"):
                subheadings.append(title.extract().strip().split(" ")[0])

            if "PvP" in subheadings:
                ratingsExist = 1
            #keeps track of the rankings per league
            rankings = {}

            #rankings exist
            if ratingsExist:

                for rankStr in response.xpath("//div[contains(@class, 'pvp-rating-expl')]//h3/descendant::text()"):
                    rankArray = rankStr.extract().strip().split(":")
                    cleanRank = rankArray[1].split("/")[0].strip()
                    rankArrayKey = rankArray[0].strip().split(" ")[0]
                    rankings[rankArrayKey] = cleanRank

                if "Great" in rankings:
                    item['greatRank'] = rankings["Great"]
                else:
                    item['greatRank'] = -1

                if "Ultra" in rankings:
                    item['ultraRank'] = rankings["Ultra"]
                else:
                    item['ultraRank'] = -1

                if "Master" in rankings:
                    item['masterRank'] = rankings["Master"]
                else:
                    item['ultraRank'] = -1

            #not pvp ranked, so rank = -1
            else:
                item['greatRank'] = -1
                item['ultraRank'] = -1
                item['masterRank'] = -1

        #If ranked in PVP, determine Pokemon's best fast and charged move
        moves = []
        if response.xpath("//div[contains(@class, 'pvp-section')]/div[contains(@class, 'pve-rating-expl')]/h3/descendant::text()").get() is None:
            item['bestMoveCombos'] = ""
        else:
            if ratingsExist:
                for mvStr in response.xpath("//div[contains(@class, 'pvp-section')]/div[contains(@class, 'pve-rating-expl')]/h3/descendant::text()"):
                    moveCombo = mvStr.extract()
                    if "+" in moveCombo:
                        moves.append(moveCombo.strip())

            movesString = ""
            for mv in moves:
                movesString = movesString+"&"+mv
            item['bestMoveCombos'] = movesString[1:]


        lastUpdatedValue = '0'
        if response.xpath("//div[contains(@class, 'last-updated-date')]/time/@datetime").get() is None:
            lastUpdatedValue = '0'
        else:
            lastUpdatedValue = response.xpath("//div[contains(@class, 'last-updated-date')]/time/@datetime").extract()
        item['lastUpdated'] = lastUpdatedValue

        print(item['name'],item['id'],item['type1'],item['type2'],item['typeAdvantages'],item['typeDisadvantages'],item['greatRank'],item['ultraRank'],item['masterRank'],item['bestMoveCombos'],item['url'], item['lastUpdated'])



        yield item

        ''' #to add later when I wanna use this info
        #item['allFastMoves'] =
        #item['allChargeMoves'] =
        #item['family'] ='''
